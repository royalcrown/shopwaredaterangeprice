<?php declare(strict_types=1);

namespace ProductDateRangePrice\Extension\Content\Product;

use ProductDateRangePrice\Core\Content\PriceDateRange\PriceDateRangeDefinition;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class PriceDateRangeExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField('dateRangePrices', PriceDateRangeDefinition::class, 'product_id', 'id'))
                ->addFlags(new CascadeDelete()),
        );
    }

    public function getDefinitionClass(): string
    {
        return ProductDefinition::class;
    }
}
