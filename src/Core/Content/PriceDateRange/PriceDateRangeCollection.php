<?php

namespace ProductDateRangePrice\Core\Content\PriceDateRange;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class PriceDateRangeCollection extends EntityCollection
{

    protected function getExpectedClass(): string
    {
        return PriceDateRangeEntity::class;
    }
}