<?php

namespace ProductDateRangePrice\Subscriber;

use Shopware\Core\System\SalesChannel\Event\SalesChannelProcessCriteriaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EntitySearchSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            'sales_channel.product.process.criteria' => 'processCriteria'
        ];
    }

    public function processCriteria(SalesChannelProcessCriteriaEvent $event): void
    {
        $event->getCriteria()->addAssociation('dateRangePrices');
    }
}
