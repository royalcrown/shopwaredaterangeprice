const { Criteria} = Shopware.Data;


Shopware.Component.override('sw-product-detail',
    {
        computed: {
            productCriteria() {
                return this.$super('productCriteria')
                    .addAssociation('dateRangePrices')
                ;
            },
        }
    });