import template from './price-date-range-form.html.twig';

import './price-date-range-form.scss'

Shopware.Component.register('price-date-range-form', {
    template,
    props: {
        dateRangePrices: {},
        currencies: {},
        defaultPrice: [],
        currencyColumns: {},
        productTaxRate: {},
        product: {},
        isPriceFieldInherited: Function,
        onInheritanceRemove: Function,
        onInheritanceRestore: Function,
    },

    inject: ['repositoryFactory', 'acl', 'feature'],

    computed: {
        dateRangePriceRepository() {
            return this.repositoryFactory.create('royal_crown_date_range_price');

        },
        pricesColumns() {
            const priceColumns = [
                {
                    property: 'fromDate',
                    label: 'royalCrown.dateRangePrices.table.from',
                    visible: true,
                    allowResize: true,
                    primary: true,
                    rawData: false,
                    width: '240px',
                }, {
                    property: 'toDate',
                    label: 'royalCrown.dateRangePrices.table.to',
                    visible: true,
                    allowResize: true,
                    primary: true,
                    rawData: false,
                    width: '220px',
                },
                {
                    property: 'type',
                    label: 'sw-product.advancedPrices.columnType',
                    visible: true,
                    allowResize: true,
                    width: '220px',
                    multiLine: true,
                },
            ];

            return [...priceColumns, ...this.currencyColumns];
        },
    },
    methods: {
        hasDateRangePrices() {
            return this.dateRangePrices.length > 0;
        },
        addDateRangePrice() {
            const dateRangePrice = this.dateRangePriceRepository.create(Shopware.Context.api);

            dateRangePrice.fromDate = (new Date()).toISOString()
            dateRangePrice.toDate = (new Date()).toISOString();
            dateRangePrice.price = [{...this.defaultPrice}];
            dateRangePrice.product = this.product;


            this.dateRangePrices.add(dateRangePrice);
        },
        deleteDateRangePrice(dateRangePrice) {

            this.dateRangePrices.remove(dateRangePrice.id);

        },
    }
});
