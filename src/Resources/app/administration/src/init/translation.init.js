import nlNLSnippets from '../../snippet/dateRangePrice.nl-NL.json';
import enGBSnippets from '../../snippet/dateRangePrice.en-GB.json';

Shopware.Locale.extend('nl-NL', nlNLSnippets);
Shopware.Locale.extend('en-GB', enGBSnippets);
