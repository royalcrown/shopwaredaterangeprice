<?php declare(strict_types=1);

namespace ProductDateRangePrice\Service;

use ProductDateRangePrice\Core\Content\PriceDateRange\PriceDateRangeEntity;
use Shopware\Core\Content\Product\SalesChannel\Price\AbstractProductPriceCalculator;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class CustomProductPriceCalculator extends  AbstractProductPriceCalculator
{

    /**
     * @var AbstractProductPriceCalculator
     */
    private AbstractProductPriceCalculator $productPriceCalculator;

    private \DateTimeImmutable $now;

    public function __construct(AbstractProductPriceCalculator $productPriceCalculator)
    {
        $this->productPriceCalculator = $productPriceCalculator;
        $this->now = new \DateTimeImmutable();
    }

    public function getDecorated(): AbstractProductPriceCalculator
    {
        return $this->productPriceCalculator;
    }

    public function calculate(iterable $products, SalesChannelContext $context): void
    {
        $this->replacePriceByValidDateRangePrice($products);
        $this->getDecorated()->calculate($products, $context);
    }

    /**
     * @param SalesChannelProductEntity[] $products
     * @return void
     */
    private function replacePriceByValidDateRangePrice(iterable $products): void
    {
        foreach ($products as $product) {
            if ($validPriceDateRange = $this->getValidDateRangePrice($product)) {
                $product->setPrice($validPriceDateRange->getPrice());
            }
        }
    }

    private function getValidDateRangePrice($product): null|PriceDateRangeEntity
    {
        if (!is_iterable($dateRangePrices = $product->getExtension('dateRangePrices'))) {
            return null;
        }

        $validPriceDateRanges = [];

        foreach ($dateRangePrices as $dateRangePrice) {
            if ($dateRangePrice->getFromDate() <= $this->now && $dateRangePrice->getToDate() >= $this->now) {
                $validPriceDateRanges[]= $dateRangePrice;
            }
        }

        if (empty($validPriceDateRanges)) {
            return null;
        }

        $this->sortOnFromDate($validPriceDateRanges);

        return $validPriceDateRanges[0];
    }

    /**
     * @param PriceDateRangeEntity[] $priceDateRanges
     * @return PriceDateRangeEntity[]
     */
    private function sortOnFromDate(array $priceDateRanges): array
    {
        usort($priceDateRanges, function($a, $b) {
            return $a->getFromDate() < $b->getFromDate();
        });

        return $priceDateRanges;
    }
}
