<?php declare(strict_types=1);

use Shopware\Core\TestBootstrapper;

$loader = (new TestBootstrapper())
    ->addCallingPlugin()
    ->addActivePlugins('ProductDateRangePrice')
    ->bootstrap()
    ->getClassLoader();

$loader->addPsr4('ProductDateRangePrice\\Tests\\', __DIR__);